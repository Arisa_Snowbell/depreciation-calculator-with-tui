use criterion::{criterion_group, criterion_main, Criterion};
use depreciation_calculator_with_tui::depreciation::{Depreciation, DepreciationGroup, DepreciationType};

fn criterion_benchmark(c: &mut Criterion) {
    let mut v = Depreciation::new(200_000, 2026, DepreciationGroup::Sixth, DepreciationType::Accelerated).unwrap();
    c.bench_function("200_000, Sixth, Accelerated", |b| b.iter(|| v.calculate()));
}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);
