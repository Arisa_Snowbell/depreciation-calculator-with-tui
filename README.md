<a href="https://repology.org/project/depreciation-calculator-with-tui/versions">
    <img src="https://repology.org/badge/vertical-allrepos/depreciation-calculator-with-tui.svg" alt="Packaging Status" align="right">
</a>

[![pipeline status](https://gitlab.com/Arisa_Snowbell/depreciation-calculator-with-tui/badges/domina/pipeline.svg)](https://gitlab.com/Arisa_Snowbell/depreciation-calculator-with-tui/)
[![](https://tokei.rs/b1/gitlab/Arisa_Snowbell/depreciation-calculator-with-tui/)](https://gitlab.com/Arisa_Snowbell/depreciation-calculator-with-tui/)


Depreciation Calculator with TUI
================================



Description
-----------

Depreciation Calculator with TUI

Preview
-------
![](preview.png)

Downloads
---------

**Releases** - The binary can be downloaded at [Release page](https://gitlab.com/Arisa_Snowbell/depreciation-calculator-with-tui/-/releases).

**Source** - The source can be downloaded as a [.compressed archive](https://gitlab.com/Arisa_Snowbell/depreciation-calculator-with-tui/-/archive/domina/depreciation-calculator-with-tui-domina.zip), or cloned from our [GitLab repo](https://gitlab.com/Arisa_Snowbell/depreciation-calculator-with-tui).

Project Members
---------------

<ul>
<li><a href="https://gitlab.com/Arisa_Snowbell">Arisa Snowbell</a> - Maintainer/Architect/Head Developer</li>
</ul>

License
-------
The license is `GNU General Public License v3.0`
