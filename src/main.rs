#![warn(clippy::all, clippy::pedantic, clippy::nursery, clippy::cargo)]
#![allow(clippy::module_name_repetitions, clippy::too_many_lines)]

use anyhow::Result;
use crossterm::{
    event::{self, DisableMouseCapture, EnableMouseCapture, Event as CEvent, KeyCode},
    execute,
    terminal::{disable_raw_mode, enable_raw_mode, EnterAlternateScreen, LeaveAlternateScreen},
};
//use chrono::{Month, Utc};
use std::{
    io, thread,
    time::{Duration, Instant},
};
use tui::{
    backend::CrosstermBackend,
    layout::{Constraint, Corner, Direction, Layout},
    style::{Color, Modifier, Style},
    text::Spans,
    widgets::{Block, Borders, Cell, List, ListItem, Row, Table},
    Terminal,
};

use crate::{
    app::App,
    depreciation::{Depreciation, DepreciationEntry, DepreciationGroup, DepreciationType},
};

mod app;
mod depreciation;
mod util;

enum Event<I> {
    Input(I),
    Tick,
}

const TICK_RATE: Duration = Duration::from_millis(400);

fn main() -> Result<()> {
    // let mut v = Depreciation::new(234567, 2012, DepreciationGroup::Fourth, DepreciationType::Uniform).unwrap();
    // v.calculate();
    // v.save_to_json_file();
    enable_raw_mode()?;

    let mut stdout = io::stdout();
    execute!(stdout, EnterAlternateScreen, EnableMouseCapture)?;
    let backend = CrosstermBackend::new(stdout);
    let mut terminal = Terminal::new(backend)?;

    // Setup input handling
    let (tx, rx) = flume::unbounded();

    terminal.clear()?;
    terminal.hide_cursor()?;

    thread::spawn(move || {
        let mut last_tick = Instant::now();
        loop {
            // poll for tick rate duration, if no events, sent tick event.
            let timeout = TICK_RATE.checked_sub(last_tick.elapsed()).unwrap_or_else(|| Duration::from_secs(0));
            if event::poll(timeout).unwrap() {
                if let CEvent::Key(key) = event::read().unwrap() {
                    tx.send(Event::Input(key)).unwrap();
                }
            }
            if last_tick.elapsed() >= TICK_RATE {
                tx.send(Event::Tick).unwrap();
                last_tick = Instant::now();
            }
        }
    });

    let mut app = App::new("Depreciation Calculator");
    let mut table_rows = Vec::new();
    loop {
        terminal.draw(|f| {
            let chunks = Layout::default()
                .direction(Direction::Horizontal)
                .constraints([Constraint::Percentage(40), Constraint::Percentage(60)].as_ref())
                .split(f.size());

            app.items.items.sort();
            app.items.items.reverse();
            let items: Vec<ListItem> = app
                .items
                .items
                .iter()
                .map(|s| ListItem::new(Spans::from(s.file_name().unwrap().to_str().unwrap().trim_end_matches(".json"))))
                .collect();

            let table = Table::new(table_rows.clone())
                .header(
                    Row::new(vec![
                        Cell::from(" Year"),
                        Cell::from("Book Price"),
                        Cell::from("Annual Depreciation"),
                        Cell::from("Total Adjustments"),
                    ])
                    .style(Style::default().fg(Color::Blue).add_modifier(Modifier::BOLD))
                    .bottom_margin(1),
                )
                .block(Block::default().borders(Borders::ALL).style(Style::default().fg(Color::White)))
                .widths(&[Constraint::Length(5), Constraint::Length(12), Constraint::Length(19), Constraint::Length(18)])
                .column_spacing(2);

            let list = List::new(items)
                .block(Block::default().borders(Borders::ALL).title(app.title))
                .highlight_symbol(">>")
                .style(Style::default().fg(Color::White))
                .start_corner(Corner::TopLeft);

            f.render_stateful_widget(list, chunks[0], &mut app.items.state);
            f.render_widget(table, chunks[1]);
        })?;

        match rx.recv()? {
            Event::Input(event) => match event.code {
                KeyCode::Char('q') => {
                    disable_raw_mode()?;
                    execute!(terminal.backend_mut(), LeaveAlternateScreen, DisableMouseCapture)?;
                    terminal.show_cursor()?;
                    break;
                }
                KeyCode::Char('a') => {
                    // TODO: POPUP MENU TO CREATE THE ENTRY
                }
                KeyCode::Left => {
                    app.on_left();
                    table_rows = Vec::new()
                }
                KeyCode::Up => {
                    app.on_up();
                    let v = app.items.state.selected();
                    if let Some(v) = v {
                        let l: Depreciation = serde_json::from_reader(std::fs::File::open(app.items.items.get(v).unwrap())?)?;
                        if let Some(l) = l.inner {
                            table_rows = l
                                .into_iter()
                                .map(|x| {
                                    Row::new(vec![
                                        Cell::from(format!(" {}", x.year.to_string())),
                                        Cell::from(format!("{} CZK", x.book_price.to_string())),
                                        Cell::from(format!("{} CZK", x.annual_depreciation.to_string())),
                                        Cell::from(format!("{} CZK", x.total_adjustments.to_string())),
                                    ])
                                })
                                .collect();
                        }
                    }
                }
                KeyCode::Right => {
                    app.on_right();
                    let v = app.items.state.selected();
                    if let Some(v) = v {
                        let l: Depreciation = serde_json::from_reader(std::fs::File::open(app.items.items.get(v).unwrap())?)?;
                        if let Some(l) = l.inner {
                            table_rows = l
                                .into_iter()
                                .map(|x| {
                                    Row::new(vec![
                                        Cell::from(format!(" {}", x.year.to_string())),
                                        Cell::from(format!("{} CZK", x.book_price.to_string())),
                                        Cell::from(format!("{} CZK", x.annual_depreciation.to_string())),
                                        Cell::from(format!("{} CZK", x.total_adjustments.to_string())),
                                    ])
                                })
                                .collect();
                        }
                    }
                }
                KeyCode::Down => {
                    app.on_down();

                    let v = app.items.state.selected();
                    if let Some(v) = v {
                        let l: Depreciation = serde_json::from_reader(std::fs::File::open(app.items.items.get(v).unwrap())?)?;
                        if let Some(l) = l.inner {
                            table_rows = l
                                .into_iter()
                                .map(|x| {
                                    Row::new(vec![
                                        Cell::from(format!(" {}", x.year.to_string())),
                                        Cell::from(format!("{} CZK", x.book_price.to_string())),
                                        Cell::from(format!("{} CZK", x.annual_depreciation.to_string())),
                                        Cell::from(format!("{} CZK", x.total_adjustments.to_string())),
                                    ])
                                })
                                .collect();
                        }
                    }
                }
                KeyCode::Char(c) => app.on_key(c),
                _ => {}
            },
            Event::Tick => {
                app.on_tick()?;
            }
        }
        if app.should_quit {
            break;
        }
    }

    Ok(())
}
