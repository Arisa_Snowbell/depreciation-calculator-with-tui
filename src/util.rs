use lazy_static::lazy_static;
use std::path::PathBuf;
use thiserror::Error;

lazy_static! {
    pub static ref BASE_PATH: PathBuf = std::env::current_exe().unwrap().parent().unwrap().to_owned();
    pub static ref DATA_BASE_PATH: PathBuf = BASE_PATH.join("data");
}

#[repr(u8)]
#[derive(Debug, Error)]
#[non_exhaustive]
pub enum Error {
    #[error("Failed to parse str to DepreciationGroup enum!")]
    FailedToParseDepreciationGroupFromStr,
    #[error("The price must be above 20 000 CZK!")]
    PriceIsTooLow,
}
