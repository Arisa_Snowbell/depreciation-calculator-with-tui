use crate::util::{Error, DATA_BASE_PATH};
use anyhow::Result;
use serde::{Deserialize, Serialize};
use smallvec::SmallVec;
use std::{fs::OpenOptions, io::Write, str::FromStr};

type DepreciationEntries = SmallVec<[DepreciationEntry; 64]>;

#[derive(Serialize, Deserialize, Debug, PartialEq, Eq)]
pub struct Depreciation {
    pub inner: Option<DepreciationEntries>,
    pub depreciation_group: DepreciationGroup,
    pub depreciation_type: DepreciationType,
    pub year_of_aquire: u16,
    pub price: u32,
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Eq)]
pub struct DepreciationEntry {
    pub year: u16,
    pub book_price: u32,
    pub annual_depreciation: u32,
    pub total_adjustments: u32,
}

impl Depreciation {
    const fn get_coefficient(&self, year: u16) -> f64 {
        match self.depreciation_type {
            DepreciationType::Uniform => match self.depreciation_group {
                DepreciationGroup::First => match year {
                    0 => 20_f64,
                    _ => 40_f64,
                },
                DepreciationGroup::Second => match year {
                    0 => 11_f64,
                    _ => 22.25,
                },
                DepreciationGroup::Third => match year {
                    0 => 5.5,
                    _ => 10.5,
                },
                DepreciationGroup::Fourth => match year {
                    0 => 2.15,
                    _ => 5.15,
                },
                DepreciationGroup::Fifth => match year {
                    0 => 1.4,
                    _ => 3.4,
                },
                DepreciationGroup::Sixth => match year {
                    0 => 1.02,
                    _ => 2.02,
                },
            },
            DepreciationType::Accelerated => match self.depreciation_group {
                DepreciationGroup::First => match year {
                    0 => 3_f64,
                    _ => 4_f64,
                },
                DepreciationGroup::Second => match year {
                    0 => 5_f64,
                    _ => 6_f64,
                },
                DepreciationGroup::Third => match year {
                    0 => 10_f64,
                    _ => 11_f64,
                },
                DepreciationGroup::Fourth => match year {
                    0 => 20_f64,
                    _ => 21_f64,
                },
                DepreciationGroup::Fifth => match year {
                    0 => 30_f64,
                    _ => 31_f64,
                },
                DepreciationGroup::Sixth => match year {
                    0 => 50_f64,
                    _ => 51_f64,
                },
            },
        }
    }

    pub const fn new(price: u32, year_of_aquire: u16, depreciation_group: DepreciationGroup, depreciation_type: DepreciationType) -> Result<Self, Error> {
        if price < 20_000 {
            return Err(Error::PriceIsTooLow);
        }

        Ok(Self {
            inner: None,
            year_of_aquire,
            depreciation_group,
            depreciation_type,
            price,
        })
    }

    pub fn calculate(&mut self) {
        let mut all_years: DepreciationEntries = DepreciationEntries::default();

        for i in 0..(self.depreciation_group as u16) {
            let prev_entry = all_years.last();
            let coefficent = self.get_coefficient(i);
            let year = self.year_of_aquire + i;

            #[allow(clippy::cast_possible_truncation, clippy::cast_sign_loss)]
            let mut annual_depreciation = match self.depreciation_type {
                DepreciationType::Uniform => (((f64::from(self.price) * coefficent) / 100_f64).ceil()) as u32,
                DepreciationType::Accelerated => match prev_entry {
                    Some(v) => (f64::from(v.book_price) * 2_f64 / (coefficent - f64::from(i))).ceil() as u32,
                    None => ((f64::from(self.price) / coefficent).ceil()) as u32,
                },
            };

            let book_price = match prev_entry {
                Some(v) => {
                    if (self.price % (self.depreciation_group as u32) != 0) && (i == (self.depreciation_group as u16) - 1) && (self.depreciation_type == DepreciationType::Uniform) {
                        annual_depreciation = v.book_price;
                    }
                    // if v.book_price < annual_depreciation {
                    //     annual_depreciation = 0;
                    // }
                    v.book_price - annual_depreciation
                }
                None => self.price - annual_depreciation,
            };

            let new_entry = DepreciationEntry {
                year,
                book_price,
                annual_depreciation,
                total_adjustments: self.price - book_price,
            };

            all_years.push(new_entry);
        }

        self.inner = Some(all_years);
    }

    /// # Errors
    /// May result in error if writing it to file fails
    pub fn save_to_json_file(&self) -> Result<()> {
        if !DATA_BASE_PATH.exists() {
            std::fs::create_dir(DATA_BASE_PATH.clone())?;
        }
        let mut file = OpenOptions::new()
            .read(true)
            .write(true)
            .create_new(true)
            .open(DATA_BASE_PATH.join(chrono::Local::now().to_rfc3339()).with_extension("json"))?;

        file.write_all(&serde_json::to_vec_pretty(self)?)?;

        Ok(())
    }
}

#[repr(u8)]
#[derive(Serialize, Deserialize, Debug, PartialEq, Eq)]
pub enum DepreciationType {
    Uniform,
    Accelerated,
    // Extraordinary, // - Needs: month_of_aquire: Month; // TODO: Support Extraordinary AKA Corona virus special Depreciation Type
}

#[repr(u8)]
#[derive(Serialize, Deserialize, Debug, PartialEq, Eq, Clone, Copy)]
pub enum DepreciationGroup {
    First = 3,
    Second = 5,
    Third = 10,
    Fourth = 20,
    Fifth = 30,
    Sixth = 50,
}

impl FromStr for DepreciationGroup {
    type Err = Error;
    fn from_str(s: &str) -> Result<Self, Error> {
        Ok(match s.to_lowercase().trim() {
            "1" | "first" | "one" => Self::First,
            "2" | "second" | "two" => Self::Second,
            "3" | "third" | "three" => Self::Third,
            "4" | "fourth" | "four" => Self::Fourth,
            "5" | "fifth" | "five" => Self::Fifth,
            "6" | "sixth" | "six" => Self::Sixth,
            _ => return Err(Error::FailedToParseDepreciationGroupFromStr),
        })
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use smallvec::smallvec;

    #[test]
    #[should_panic]
    fn weird_case() {
        let mut v = Depreciation::new(5432, 2012, DepreciationGroup::Fourth, DepreciationType::Uniform).unwrap();
        v.calculate();

        let k: DepreciationEntries = smallvec![
            DepreciationEntry {
                year: 2012,
                book_price: 5315,
                annual_depreciation: 117,
                total_adjustments: 117
            },
            DepreciationEntry {
                year: 2013,
                book_price: 5035,
                annual_depreciation: 280,
                total_adjustments: 397
            },
            DepreciationEntry {
                year: 2014,
                book_price: 4755,
                annual_depreciation: 280,
                total_adjustments: 677
            },
            DepreciationEntry {
                year: 2015,
                book_price: 4475,
                annual_depreciation: 280,
                total_adjustments: 957
            },
            DepreciationEntry {
                year: 2016,
                book_price: 4195,
                annual_depreciation: 280,
                total_adjustments: 1237
            },
            DepreciationEntry {
                year: 2017,
                book_price: 3915,
                annual_depreciation: 280,
                total_adjustments: 1517
            },
            DepreciationEntry {
                year: 2018,
                book_price: 3635,
                annual_depreciation: 280,
                total_adjustments: 1797
            },
            DepreciationEntry {
                year: 2019,
                book_price: 3355,
                annual_depreciation: 280,
                total_adjustments: 2077
            },
            DepreciationEntry {
                year: 2020,
                book_price: 3075,
                annual_depreciation: 280,
                total_adjustments: 2357
            },
            DepreciationEntry {
                year: 2021,
                book_price: 2795,
                annual_depreciation: 280,
                total_adjustments: 2637
            },
            DepreciationEntry {
                year: 2022,
                book_price: 2515,
                annual_depreciation: 280,
                total_adjustments: 2917
            },
            DepreciationEntry {
                year: 2023,
                book_price: 2235,
                annual_depreciation: 280,
                total_adjustments: 3197
            },
            DepreciationEntry {
                year: 2024,
                book_price: 1955,
                annual_depreciation: 280,
                total_adjustments: 3477
            },
            DepreciationEntry {
                year: 2025,
                book_price: 1675,
                annual_depreciation: 280,
                total_adjustments: 3757
            },
            DepreciationEntry {
                year: 2026,
                book_price: 1395,
                annual_depreciation: 280,
                total_adjustments: 4037
            },
            DepreciationEntry {
                year: 2027,
                book_price: 1115,
                annual_depreciation: 280,
                total_adjustments: 4317
            },
            DepreciationEntry {
                year: 2028,
                book_price: 835,
                annual_depreciation: 280,
                total_adjustments: 4597
            },
            DepreciationEntry {
                year: 2029,
                book_price: 555,
                annual_depreciation: 280,
                total_adjustments: 4877
            },
            DepreciationEntry {
                year: 2030,
                book_price: 275,
                annual_depreciation: 280,
                total_adjustments: 5157
            },
            DepreciationEntry {
                year: 2031,
                book_price: 0,
                annual_depreciation: 275,
                total_adjustments: 5432
            }
        ];
        assert_eq!(v.inner.unwrap(), k)
    }

    #[test]
    fn accelerated() {
        let mut v = Depreciation::new(50_000, 2026, DepreciationGroup::Second, DepreciationType::Accelerated).unwrap();
        v.calculate();

        let k: DepreciationEntries = smallvec![
            DepreciationEntry {
                year: 2026,
                book_price: 40000,
                annual_depreciation: 10000,
                total_adjustments: 10000,
            },
            DepreciationEntry {
                year: 2027,
                book_price: 24000,
                annual_depreciation: 16000,
                total_adjustments: 26000,
            },
            DepreciationEntry {
                year: 2028,
                book_price: 12000,
                annual_depreciation: 12000,
                total_adjustments: 38000,
            },
            DepreciationEntry {
                year: 2029,
                book_price: 4000,
                annual_depreciation: 8000,
                total_adjustments: 46000,
            },
            DepreciationEntry {
                year: 2030,
                book_price: 0,
                annual_depreciation: 4000,
                total_adjustments: 50000,
            },
        ];

        assert_eq!(v.inner.unwrap(), k)
    }

    #[test]
    fn uniform() {
        let mut v = Depreciation::new(50_000, 2026, DepreciationGroup::Second, DepreciationType::Uniform).unwrap();
        v.calculate();

        let k: DepreciationEntries = smallvec![
            DepreciationEntry {
                year: 2026,
                book_price: 44500,
                annual_depreciation: 5500,
                total_adjustments: 5500,
            },
            DepreciationEntry {
                year: 2027,
                book_price: 33375,
                annual_depreciation: 11125,
                total_adjustments: 16625,
            },
            DepreciationEntry {
                year: 2028,
                book_price: 22250,
                annual_depreciation: 11125,
                total_adjustments: 27750,
            },
            DepreciationEntry {
                year: 2029,
                book_price: 11125,
                annual_depreciation: 11125,
                total_adjustments: 38875,
            },
            DepreciationEntry {
                year: 2030,
                book_price: 0,
                annual_depreciation: 11125,
                total_adjustments: 50000,
            },
        ];

        assert_eq!(v.inner.unwrap(), k)
    }
}
