use std::{io, path::PathBuf};

use crate::util::DATA_BASE_PATH;
use anyhow::Result;
use tui::widgets::ListState;

pub struct App<'a> {
    pub title: &'a str,
    pub should_quit: bool,
    pub items: StatefulList<PathBuf>,
}

impl<'a> App<'a> {
    pub fn new(title: &'a str) -> App<'a> {
        App {
            title,
            should_quit: false,
            items: StatefulList::new(),
        }
    }

    pub fn on_up(&mut self) {
        if !self.items.items.is_empty() {
            self.items.previous();
        }
    }

    pub fn on_down(&mut self) {
        if !self.items.items.is_empty() {
            self.items.next();
        }
    }

    pub fn on_right(&mut self) {
        if !self.items.items.is_empty() {
            self.items.next()
        }
    }
    pub fn on_left(&mut self) {
        self.items.unselect();
    }

    pub fn on_key(&mut self, c: char) {
        match c {
            'q' => {
                self.should_quit = true;
            }
            _ => {}
        }
    }

    pub fn on_tick(&mut self) -> Result<()> {
        // update entries
        if DATA_BASE_PATH.exists() {
            self.items.items = std::fs::read_dir(DATA_BASE_PATH.clone())?
                .map(|res| res.map(|e| e.path()))
                .filter(|e| e.as_ref().unwrap().extension().unwrap().to_str().unwrap() == "json")
                .collect::<Result<Vec<_>, io::Error>>()?;
        } else {
            std::fs::create_dir_all(DATA_BASE_PATH.clone())?;
        }
        Ok(())
    }
}

pub struct StatefulList<T> {
    pub state: ListState,
    pub items: Vec<T>,
}

impl<T> StatefulList<T> {
    pub fn new() -> Self {
        Self {
            state: ListState::default(),
            items: Vec::new(),
        }
    }

    pub fn with_items(items: Vec<T>) -> Self {
        Self { state: ListState::default(), items }
    }

    pub fn next(&mut self) {
        let i = match self.state.selected() {
            Some(i) => {
                if i >= self.items.len() - 1 {
                    0
                } else {
                    i + 1
                }
            }
            None => 0,
        };
        self.state.select(Some(i));
    }

    pub fn previous(&mut self) {
        let i = match self.state.selected() {
            Some(i) => {
                if i == 0 {
                    self.items.len() - 1
                } else {
                    i - 1
                }
            }
            None => 0,
        };
        self.state.select(Some(i));
    }

    pub fn unselect(&mut self) {
        self.state.select(None);
    }
}
